= How to build derived bootc container images

The original Docker container model of using "layers" to model
applications has been extremely successful. This project
aims to apply the same technique for bootable host systems - using
standard OCI/Docker containers as a transport and delivery format
for base operating system updates.

== Lifecycle binding code and configuration

At the current time, the role of `bootc` is solely to boot and
upgrade from a single container image. This is a very simplistic
model, but it is one that captures many use cases.

In particular, the default assumption is that *code* and *configuration*
for the base OS are tightly bound. Systems which update
one or the other asynchronously often lead to problems with
skew.

=== Containerized vs 1:1 host:app

A webserver is the classic case of something that can be run
as a container on a generic host alongside other workloads. 
However, many systems today still follow a "1:1" model between application
and a virtual machine. Migrating to a container build for
this can be an important stepping stone into eventually
lifting the workload into an application container itself.

Additionally in practice, even some containerized workloads
have such strong bindings/requirememnts for the host system
that they effectively require a 1:1 binding. Production
databases often fall into this class.

=== httpd (bound)

Nevertheless, here's a classic static http webserver example;
an illustrative aspect is that we move content from `/var` into `/usr`.
It expects an `index.html` colocated with the Containerfile.

.Containerfile
[source,dockerfile,subs="attributes"]
----
FROM {container-fedora-full}
# The default package drops content in /var/www, and on bootc systems
# we have /var as a machine-local mount by default. Because this content
# should be read-only (at runtime) and versioned with the container image,
# we move it to /usr/share/www instead.
RUN dnf -y install httpd && \
    systemctl enable httpd && \
    mv /var/www /usr/share/www && \
    echo 'd /var/log/httpd 0700 - - -' > /usr/lib/tmpfiles.d/httpd-log.conf && \
    sed -ie 's,/var/www,/usr/share/www,' /etc/httpd/conf/httpd.conf
# Further, we also disable the default index.html which includes the operating
# system information (bad idea from a fingerprinting perspective), and crucially
# we inject our own content as part of the container image build.
# This is a key point: In this model, the webserver content is lifecycled exactly
# with the container image build, and you can change it "day 2" by updating
# the image. The content is underneath the /usr readonly bind mount - it
# should not be mutated per machine.
RUN rm /usr/share/httpd/noindex -rf
COPY index.html /usr/share/www/html
EXPOSE 80
----

=== httpd (containerized)

In contrast, this example demonstrates a webserver
as a "referenced" container image via
{podman-docs}/podman-systemd.unit.5.html[podman-systemd]
that is also configured for automatic updates.

[subs="attributes"]
--
This reference example is maintained in {git-examples}/app-podman-systemd[app-podman-systemd].
--

.caddy.container
[source]
----
[Unit]
Description=Run a demo webserver

[Container]
# This image happens to be multiarch and somewhat maintained
Image=docker.io/library/caddy
PublishPort=80:80
AutoUpdate=registry

[Install]
WantedBy=default.target
----

.Containerfile
[source,dockerfile,subs="attributes"]
----
# In this example, a simple "podman-systemd" unit which runs
# an application container via {podman-docs}/podman-systemd.unit.5.html
# that is also configured for automatic updates via
# {podman-docs}/podman-auto-update.1.html
FROM {container-c9s}
COPY caddy.container /usr/share/containers/systemd
# Enable the simple "automatic update containers" timer, in the same way
# that there is a simplistic bootc upgrade timer. However, you can
# obviously also customize this as you like; for example, using
# other tooling like Watchtower or explicit out-of-band control over container
# updates via e.g. Ansible or other custom logic.
RUN systemctl enable podman-auto-update.timer
----

== Authentication, users and groups

The container images above are just illustrative demonstrations that
are not useful standalone. It is highly likely that you will want to
run other container images, and perform other customizations.

Among the most likely additions is configuring a mechanism for remote
SSH; see xref:authentication.adoc[Authentication, Users, and Groups].

== General configuration guidance

See the https://containers.github.io/bootc/building/guidance.html[bootc upstream guidance].

Many configuration changes to a Linux system boil down effectively to
writing configuration files into `/etc` or `/usr` - those operations
translate seamlessly into booted hosts via a `COPY` instruction
or similar in a container build.

== More examples

See {git-examples}[Examples] for many examples of
container image definitions!
