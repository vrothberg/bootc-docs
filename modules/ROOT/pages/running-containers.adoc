= Running Containers

== Introduction

{projname} ships with https://podman.io[podman] installed to run application containers.

Per the https://containers.github.io/bootc/building/guidance.html#configuration[configuration guidance]
prefer using `/usr/lib/containers/systemd` for including "static" container images that are launched
dynamically.

== Example configuration (caddy)

The following configures the systemd `caddy.service` to run https://www.busybox.net[busybox].

This is one of several examples in {git-examples}[Fedora/CentOS bootc examples].

{podman-docs}/podman-systemd.unit.5.html[Podman systemd] is functionality included in podman that allows starting containers via systemd using a systemd generator.

[source,text]
----
$ cat /usr/lib/containers/systemd/caddy.image
[Image]
Name=docker.io/library/caddy
$ cat /usr/lib/containers/systemd/caddy.container
[Unit]
Description=Run a demo webserver

[Container]
# This image happens to be multiarch and somewhat maintained
Image=docker.io/library/caddy
PublishPort=80:80
AutoUpdate=registry

[Install]
WantedBy=default.target
----

== Lifecycling and updating containers (separate)

Note that that the example above uses an implicit floating `:latest`
tag. When configured this way, by default the specified image will be pulled
once at install time and not updated thereafter. This is rarely
desirable; however, the choice of how to update the application
containers is one that needs to be designed to match the specific
workload.

=== Note: Images are still separate

In all models described below, the "application" containers
are pushed and managed separately from the base bootc image.
They are fetched dynamically over the network - or in other
words, they are "physically" distinct from the base bootc image.

See xref:embedding-containers.adoc[Embedding containers] for
a model where the application workloads are physically embedded
in the base bootc image.

=== Lifecycle binding containers by default via tags or digests

Instead of referencing a container by a generic floating tag
such as `:latest`, instead you can include dedicated versioned
tags or `sha256` digest inside the referenced container image.

The example `caddy` container has tags of the form `:2.6`, `:2.7`
etc. If you specify one of those via e.g.

```
Image=docker.io/library/caddy:2.7
```

Then you can change them in your "base bootc" container image definition,
and have them updated transactionally at the same time as
the host system.

The state of the system (including these workload containers) is then describeable via the single
base bootc container image.

=== Enabling asynchronous automatic updates for container images

In many cases however, you will want the ability to have at least some
application containers "float" versions distinct from the host system. 

The {podman-docs}/podman-auto-update.1.html[podman-auto-update.timer]
unit can be enabled to automatically upgrade workload container images that
are explicitly configured to opt-in to automatic updates.

== Dynamic containers

Updating container images dynamically, distinct from the base OS image is
one case of xref:dynamic-reconfiguration.adoc[Dynamic reconfiguration].
